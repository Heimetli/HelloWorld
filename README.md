# Git Beispiel

## Repository erzeugen

    C:\ffhs\FTOOP\Git\HelloWorld>git init
    Initialized empty Git repository in C:/ffhs/FTOOP/Git/HelloWorld/.git/

## Status checken

    C:\ffhs\FTOOP\Git\HelloWorld>git status
    On branch master
    
    Initial commit
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

        HelloWorld.class
        HelloWorld.java

    nothing added to commit but untracked files present (use "git add" to track)

Die .class Files will ich nicht im Repository, deshalb mit einem .gitignore ausblenden

## .gitignore erstellen

    C:\ffhs\FTOOP\HelloWorld>notepad .gitignore

Den Inhalt dieses Files finden Sie im Repository

## Kontrolle

    C:\ffhs\FTOOP\Git\HelloWorld>git status
    On branch master
    
    Initial commit
    
    Untracked files:
      (use "git add <file>..." to include in what will be committed)

        .gitignore
        HelloWorld.java

    nothing added to commit but untracked files present (use "git add" to track)

## Die Files in die Staging Area aufnehmen

    C:\ffhs\FTOOP\Git\HelloWorld>git add .gitignore HelloWorld.java

## Prüfen ob's geklappt hat

    C:\ffhs\FTOOP\Git\HelloWorld>git status
    On branch master
    
    Initial commit

    Changes to be committed:
      (use "git rm --cached <file>..." to unstage)

        new file:   .gitignore
        new file:   HelloWorld.java

## Einchecken ins lokale Repository

    C:\ffhs\FTOOP\Git\HelloWorld>git commit -m "Initial commit"
    [master (root-commit) 027de46] Initial commit
     2 files changed, 5 insertions(+)
     create mode 100644 .gitignore
     create mode 100644 HelloWorld.java
     
## Was meldet status jetzt?

    C:\ffhs\FTOOP\Git\HelloWorld>git status
    On branch master
    nothing to commit, working tree clean

## Mit dem Repository auf gitlab verknüpfen

    C:\ffhs\FTOOP\Git\HelloWorld>git remote add origin https://gitlab.com/Heimetli/HelloWorld.git

## Ins gitlab-Repository pushen

    C:\ffhs\FTOOP\Git\HelloWorld>git push -u origin master
    Counting objects: 4, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (2/2), done.
    Writing objects: 100% (4/4), 310 bytes | 0 bytes/s, done.
    Total 4 (delta 0), reused 0 (delta 0)
    Branch master set up to track remote branch master from origin.
    To https://gitlab.com/Heimetli/HelloWorld.git
     * [new branch]      master -> master

## File editieren und dann den Status checken

    C:\ffhs\FTOOP\Git\HelloWorld>git status
    On branch master
    Your branch is up-to-date with 'origin/master'.
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   HelloWorld.java

    no changes added to commit (use "git add" and/or "git commit -a")

## Das File in die Staging Area aufnehmen

    C:\ffhs\FTOOP\Git\HelloWorld>git add HelloWorld.java

## Einchecken

    C:\ffhs\FTOOP\Git\HelloWorld>git commit -m "Added main"
    [master cc3c38b] Added main
     1 file changed, 3 insertions(+)

## File weiter editieren, Adden und Einchecken in einem Befehl

   C:\ffhs\FTOOP\Git\HelloWorld>git commit -a -m "Finished"
   [master ab0aa57] Finished
    1 file changed, 1 insertion(+)

## Und wieder mal auf den Server damit

    C:\ffhs\FTOOP\Git\HelloWorld>git push
    Counting objects: 6, done.
    Delta compression using up to 2 threads.
    Compressing objects: 100% (6/6), done.
    Writing objects: 100% (6/6), 655 bytes | 0 bytes/s, done.
    Total 6 (delta 1), reused 0 (delta 0)
    To https://gitlab.com/Heimetli/HelloWorld.git
       3693f66..ab0aa57  master -> master








































